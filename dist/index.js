"use strict";

var _first = _interopRequireDefault(require("@testrepo/first"));

var _second = _interopRequireDefault(require("@testrepo/second"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var app = function app() {
  return "hello from app";
};

console.log(app());
console.log((0, _first["default"])());
console.log((0, _second["default"])());
console.log("end"); // const first = require('@testrepo/first');
// const second = require('@testrepo/second');
//
// const app = () => 'hello from app';
//
// (() => {
//     console.log(app());
//     console.log(first());
//     console.log(second());
// })();