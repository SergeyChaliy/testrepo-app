import first from "@testrepo/first";
import second from "@testrepo/second";

const app = () => "hello from app";

console.log(app());
console.log(first());
console.log(second());
console.log("end");

// const first = require('@testrepo/first');
// const second = require('@testrepo/second');
//
// const app = () => 'hello from app';
//
// (() => {
//     console.log(app());
//     console.log(first());
//     console.log(second());
// })();
